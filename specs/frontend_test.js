const { openBrowser, goto, write, click, press, near, checkBox, closeBrowser, $, doubleClick } = require('taiko');
(async () => {
    try {
        await openBrowser();
        await goto("http://127.0.0.1:8080/",{navigationTimeout:30000});
        await write('First item');
        await press('Enter');
        await write('Second item');
        await press('Enter');
        await write('Third item');
        await press('Enter');
        await write('Fourth item');
        await press('Enter');
        await click(checkBox(near("First item")));
        await click(checkBox(near("Third item")));
        await click('Active');
        await click('Completed');
        await click('Clear completed');
        await click('All');
        await click($("#toggle-all"));
        await click($("#toggle-all"));
        await doubleClick('Second item');
        await clear();
        await write("Cake");
        await press("Enter");
        await click(checkBox(near("Fourth item")));
        await click("Clear completed");
        await write("Test is end");
        await press("Enter");
    } catch (error) {
        console.error(error);
    } finally {
        await closeBrowser();
    }
})();
