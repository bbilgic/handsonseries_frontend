# Todo test

## Go to App
* Go to todo app page

## Add Items

* Add todo item "First item"
* Add todo item "Second item"
* Add todo item "Third item"
* Add todo item "Fourth item"

## Complete Items

* Click on todo "First item"
* Click on todo "Third item"
* Clear complete

## Update Items

* Update item "Second item" to "Cake"


## Happy End!

* Click on todo "Fourth item"
* Click on todo "Cake"
* Clear complete
* Add todo item "Happy End!"

