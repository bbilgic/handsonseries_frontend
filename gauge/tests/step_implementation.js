    /* globals gauge*/
"use strict";
const { openBrowser, goto, write, click, press, near, checkBox, closeBrowser, $, doubleClick ,clear} = require('taiko');

const timeout = ms => new Promise(res => setTimeout(res, ms))


beforeSuite(async () => {
    await openBrowser({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] })
});

afterSuite(async () => {
    await timeout(3000)
    await closeBrowser();
});

step("Go to todo app page", async () => {
    await goto("http://127.0.0.1:8080/");
});

step("Add todo item <todoItem>", async (todoItem) => {
    await write(todoItem);
    await press('Enter');
});

step("Clear complete", async () => {
    await click('Clear completed');
})


step("Delete todo item <todoItem>", async (todoItem) => {
    await click(button('Delete', toRightOf(todoItem)));
});

step("Click on todo <todoItem>", async (todoItem) => {
    await click(checkBox(near(todoItem)));
})

step("Todo <todoItem> should be completed", async (todoItem) => {
    const completedTodos = await $("table tr.todo-completed td:first-child").text();
    assert.include(completedTodos, todoItem);
})

step("Page contains <content>", async (content) => {
    assert.isTrue(await text(content).exists());
});

step("Todo list contains <expectedTodo>", async (expectedTodo) => {
    const todos = await $("table tr td:first-child").text();
    assert.include(todos, expectedTodo);
});

step("Todo list not contains <expectedTodo>", async (expectedTodo) => {
    const todos = await $("table tr td:first-child").text();
    assert.notInclude(todos, expectedTodo);
});

step("Wait <s> seconds", async (seconds) => {
    await timeout(parseInt(seconds) * 1000)
})



step("Update item <todoItem> to <NewtodoItem>", async (todoItem,NewtodoItem) => {
    await doubleClick(todoItem);
    await clear();
    await write(NewtodoItem);
    await press("Enter");
})


